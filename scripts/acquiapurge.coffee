# Description:
#   Hubot purges a url from the the varnish cache on Acquia hosting.
#
# Supported sites:
#   coloradospr - coloradosprings.gov
#
# Dependencies:
#   none
#
# Commands:
#   hubot acquia purge <site> <url> - Purge <url> from Acquia's varnish cache
#
# Author: draenen <caleb@calebthorne.com>
# Date: Dec 9, 2015
#

module.exports = (robot) ->
  exclamation = [
    "Bam!"
    "Bazinga!"
    "Eureka!"
    "Gadzooks!"
    "Poof!"
    "Presto!"
    "Ta-Da!"
    "Yepperdoo!"
    "Voilà!"
    "Yeehaw!"
  ]

  acquia_purge = (site, host, url, msg) ->
    util = require('util')
    exec = require('child_process').exec;
    cmd = "curl -L -X PURGE -H \"X-Acquia-Purge:#{site}\" --compress -H \"Host: #{host}\" #{url}"
    exec cmd, (err, stdout, stderr) ->
      if err isnt null
        msg.send "Error: Could not purge the Acquia varnish cache for #{host} #{url}"
        robot.logger.warning("Could not purge Acquia cache for #{host} #{url}. Error: #{err}")

  robot.respond /acquia purge ([a-z]+) ((http[s]?:\/\/)?([a-z0-9\-\.]+)(\/.*))/i, (msg) ->
    site = msg.match[1]
    full = msg.match[2]
    host = msg.match[4]
    path = msg.match[5]

    if site is "coloradospr"
      srvs = [
        "http://bal-7943.prod.hosting.acquia.com"
        "http://bal-7944.prod.hosting.acquia.com"
      ]
      for srv in srvs
        url = "#{srv}#{path}"
        acquia_purge site, host, url, msg
      msg.send (msg.random exclamation) + " Cache busted for #{full} on #{site}."

    else
      msg.reply "Sorry, I can't find the #{site} Acquia site."
