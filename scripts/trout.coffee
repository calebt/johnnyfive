# Description
#   Fish slap your friends.
#
# Commands:
#   trout <thing> - Slaps <thing> with a big fish

nope = [
  "http://cdn.meme.am/instances/400x/54668412.jpg",
  "http://cdn.meme.am/instances/500x/45625264.jpg",
  "http://static.fjcdn.com/comments/Nice+try+i+learned+my+listen+3+times+today+_531f904517fb0b5eeee5c76297303f0c.jpg",
  "http://4.bp.blogspot.com/-eDYBSSXIm1g/UC1K2_0GDaI/AAAAAAAAA04/c2HiVDJyKP0/s1600/there+is+nothing+to+quot+feel+quot+here.+nice+try+though+_4dc54311698b7393680634b9179cb597.jpg",
  "Yeah, um no."
]

module.exports = (robot) ->
  robot.hear /trout (@?([^\s\\]+))/i, (msg) ->
    name = msg.match[2]
    value = msg.match[1]
    if name == robot.name
      msg.send msg.random nope
    else
      msg.send "/me slaps #{value} around a bit with a large trout."

  robot.respond /trout\?/i, (msg) ->
    msg.send "https://www.youtube.com/watch?v=WsfiD78Cy0s"
