# Description:
#   Hubot tells you when Drupal 8 will be released.
#
# Dependencies:
#   "jsdom": "3"
#   "request": ""
#
# Commands:
#   hubot d[rupal ]8 release date? - hubot is smart. hubot knows.
#
# Author: Draenen (Caleb Thorne)
#
#

request = require 'request'
jsdom = require 'jsdom'
jquery = 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'

months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

days = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Thursday',
  'Friday',
  'Saturday'
]

module.exports = (robot) ->
  fetchDate = (msg) ->
    msg.http("https://drupalreleasedate.com/data/estimates.json")
      .get() (err, res, body) ->
        try
          data = JSON.parse(body).data
          date = new Date(data[data.length-1].estimate)
          msg.send 'The current estimate for Drupal 8 release is ' + days[date.getUTCDay()] + ', ' + months[date.getUTCMonth()] + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear()

        catch error
          robot.logger.warning("Could not fetch drupal release date data.")

  robot.respond /d(rupal )?8 release( date)?\??/i, fetchDate

  robot.hear /https:\/\/drupalreleasedate.com/i, fetchDate
