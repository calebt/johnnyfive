# Description:
#   Hubot responds politly to messages.
#
# Commands:
#   hello|hi - Hubot returns your greeting
#   thank[s] [you] - Hubot accepts your thanks
#   [good]bye - Hubot bids you farwell.
#

hello = [
  "Hi",
  "Hello",
  "Hey",
  "Hi, how are ya?",
  "Hey, how’s it goin’?",
]

thanks = [
  "You're welcome",
  "No problem",
  "Not a problem",
  "No problem at all",
  "Don’t mention it",
  "It’s no bother",
  "It’s my pleasure",
  "My pleasure",
  "It’s nothing",
  "Think nothing of it",
  "No, no. Thank you!",
  "Sure thing"
]

bye = [
  "See you later.",
  "Catch you later.",
  "Take care.",
  "Farewell.",
  "Have a good day.",
  "Bye!",
  "Bye bye!",
  "Later!",
  "Have a good one.",
  "So long.",
  "Au revoir.",
  "Sayonara!"
]

module.exports = (robot) ->
  robot.hear /\b(hello|hi)\b/i, (msg) ->
    msg.send msg.random hello

  robot.hear /\b(good morning)\b/i, (msg) ->
    msg.reply "What do you mean? Do you wish me a good morning, or mean that it is a good morning whether I want it or not; or that you feel good this morning; or that it is a morning to be good on?"

  robot.respond /\bthanks?\b/i, (msg) ->
    msg.send msg.random thanks

  robot.hear /\b(good)?bye\b/i, (msg) ->
    msg.send msg.random bye
